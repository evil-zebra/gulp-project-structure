# Getting Started
Simple run `gulp` to execute the gulpfile tasks.

# What Is This Repository?
This was created for [a blog post showcasing how to use Gulp to organize a project](https://evil-zebra.com/using-gulp-to-organize-and-streamline-any-development-project/). The repository can also be used as an (almost) empty project structure for new projects.