// require any modules needed
var gulp   = require('gulp');
var uglify = require('gulp-uglify');

// move html files
gulp.task('move', function() {
    // logic for this task
    return gulp.src('src/*.html')
        .pipe( gulp.dest('dist') )
});

// uglify javascript files
gulp.task('uglifyjs', function() {
    // logic for this task
    return gulp.src('src/*.js')
        .pipe( uglify() )
        .pipe( gulp.dest('dist') )
});

// setup default task, these dependencies can be run in parallel
gulp.task('default', ['uglifyjs', 'move']);